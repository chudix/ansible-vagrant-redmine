# Lab 1: Redmine

Instalar con Ansible en Vagrant un Redmine: el SO subyacente y servicios
empleados deben aplicar a buenas prácticas de seguridad. Por ejemplo: cerrar
puertos que no se usen, que los servicios corran con usuarios que no sean
privilegiados, servicios que no expongan versiones o capacidades innecesarias.

* El desarrollo debe poder ser probado con Vagrant.
* Si se utiliza TDD de los playbooks desarrollados, mejor.
* Para el despliegue, no se podrá emplear Docker.

Se recuerda que siempre es importante analizar el problema de manera detallada,
calcular el tiempo estimado que llevaría finalizarlo (en horas) que debe ser
colocado en el campo "Tiempo estimado" del ticket. Además, en caso de ser
necesario, se deben explotar los tickets en nuevos tickets de menor alcance
y relacionarlos entre sí.
Otro punto de relevancia es que se debe documentar de forma detallada,
los pasos seguidos para llegar a cada solución.

## TDD

Lo primero por realizar es una lista de todos los requerimientos que
tenemos, así poder elegir alguno y pensar como probarlo. Luego
podré escribir los tests, ver que estos no se satisfacen y por
último escribir la implementación para que pasen.

### Elicitación de requerimientos

* Buenas practicas de seguridad
  
  * Solo los puertos usados deben estar abiertos.
  
  * Servicios deben correr con usuarios no privilegiados
  
  * Servicios no deber exponer versiones o capacidades innecesarias.

* El sistema debe correr un Redmine

* Se debe utilizar Vagrant y Ansible

* No se debe utilizar Docker.

### Sobre seguridad

Esta es una lista de la información recopilada acerca de seguridad, a tener en
cuenta para el desarrollo. Una vez que se tenga el sistema funcionando
se puede ahondar más en el tema para obtener ideas y así escribir
tests específicos que prueben vulnerabilidades.

* [Security Best practices de Ansible](https://docs.ansible.com/ansible-tower/latest/html/administration/security_best_practices.html)
* [Guía de seguridad de RedHat: Evaluación de vulnerabilidades](https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/7/html/security_guide/sec-vulnerability_assessment): Quizás deba usar herramientas como `nmap` para verificar el estado de los puertos.
* [Guía de seguridad de RedHat: Controlar el acceso `root`](https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/7/html/security_guide/sec-controlling_root_access)
  * Verificar que estén bien configurados servicios como `cups` y `sshd`.
  * no utilizar servicios inseguros como `rlogin`, `rsh`, `telnet`, y `vsftpd`.
* [Guía de seguridad de RedHat: Servicios](https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/7/html/security_guide/sec-securing_services)
* [Checklist básico de Seguridad en Redmine](https://www.easyredmine.com/documentation-of-easy-redmine?view=knowledge_detail&id=214)

### Sobre tests

Según lo investigado la herramienta más usada para correr tests en Ansible es
[Molecule](https://molecule.readthedocs.io/en/latest/).
Funciona con `python` por lo que será mejor separar un entorno específico
para este desarrollo utilizando `venv`. Luego de creado el entorno virtual
se podrá instalar en él el `ansible`, `molecule`, y `python-vagrant` que
es para usar Vagrant como *driver* (por defecto es Docker). Una vez que
se tengan todas las dependencias instaladas se pueden guardar con
`pip freeze`.

### Sobre Redmine

Mirando la [Guía de instalación de Redmine](https://www.redmine.org/projects/redmine/wiki/RedmineInstall)
veo que antes es necesario tener Ruby instalado en el sistema, para
lo cual sera necesario instalar `rvm` o `rbenv` para manejar las
versiones.

## Notas de desarrollo

Como primer paso voy a instalar los requisitos antes mencionados e intentar
correr un test básico. Luego el objetivo es poder escribir un test que
verifique que no está instalado Ruby (o rbenv, por ejemplo).

```sh
$ sudo apt install python3, python3-venv # por si no están
$ python3 -m venv venvtest
$ source venvtest/bin/activate
(venvtest) $ pip install ansible
(venvtest) $ pip install molecule
(venvtest) $ pip install python-vagrant
```

```bash
(venvtest)$ molecule init role --role-name rbenv
Usage: molecule init role [OPTIONS] ROLE_NAME
Try 'molecule init role --help' for help.

Error: No such option: --role-name (Possible options: --driver-name, --lint-name, --provisioner-name)
```

Ya arrancamos mal, se ve que cambió el comando, ya no se usa el parámetro `--role-name`.
Además hay que indicar el *driver* con `-d`

```sh
(venvtest)$ molecule init role example -d vagrant
Usage: molecule init role [OPTIONS] ROLE_NAME
Try 'molecule init role --help' for help.

Error: Invalid value for '--driver-name' / '-d': 'vagrant' is not 'delegated'.
```

Parece que hay que instalar también `molecule-vagrant` para poder usar ese driver

```sh
(venvtest) $ pip install molecule-vagrant
(venvtest) $ molecule init role example -d vagrant
INFO     Initializing new role example...
Using /etc/ansible/ansible.cfg as config file
- Role example was created successfully
INFO     Initialized role in /home/lab1/prueba/example successfully.
```

Ahora con `molecule test` deberían correrse los tests y pasar todos, ya que están
vacíos.

```sh
(env-lab1) $ molecule test
...
...
PLAY RECAP *********************************************************************
localhost                  : ok=3    changed=2    unreachable=0    failed=0    skipped=0    rescued=0    ignored=0

INFO     Pruning extra files from scenario ephemeral directory
```

Ahora debería poder escribir los test en python pero no encuentro la carpeta
donde deberían estar.
Es porque no indiqué que verificador debe usar el rol.

```sh
(venvtest) $ rm -rf example
(venvtest) $ molecule init role example -d vagrant --verifier-name testinfra
INFO     Initializing new role example...
Using /etc/ansible/ansible.cfg as config file
- Role example was created successfully
INFO     Initialized role in /home/lab1/prueba/example/example successfully.
```

Entonces ahora en `example/molecule/default/tests/test_default.py`

```python
def test_ruby_binary(host):
    f = host.file('/usr/bin/ruby')
    assert f.exists
```

Este test debería fallar.

Falla pero no por lo que se esperaba.

```sh
INFO     Running default > verify
INFO     Executing Testinfra tests found in /home/lab1/prueba/example/molecule/default/tests/...
/bin/bash: pytest: command not found
```

Instalo `pip install pytest testinfra`

```sh
INFO     Executing Testinfra tests found in /home/lab1/prueba/example/molecule/default/tests/...
============================= test session starts ==============================
platform linux -- Python 3.8.6, pytest-6.2.4, py-1.10.0, pluggy-0.13.1
rootdir: /home
plugins: testinfra-6.3.0, testinfra-6.0.0
collected 2 items

molecule/default/tests/test_default.py .F                                [100%]

=================================== FAILURES ===================================
_____________________ test_ruby_binary[ansible://instance] _____________________

host = <testinfra.host.Host ansible://instance>

    def test_ruby_binary(host):
        """Validate ruby binary file exists."""
        f = host.file('/usr/bin/ruby')
>       assert f.exists
E       assert False
E        +  where False = <file /usr/bin/ruby>.exists

molecule/default/tests/test_default.py:15: AssertionError
=========================== short test summary info ============================
FAILED molecule/default/tests/test_default.py::test_ruby_binary[ansible:/instance]
========================= 1 failed, 1 passed in 3.13s ==========================
/home/lab1/venvtest/lib/python3.8/site-packages/_testinfra_renamed.py:5: DeprecationWarning: testinfra package has been renamed to pytest-testinfra. Please `pip install pytest-testinfra` and `pip uninstall testinfra` and update your package requirements to avoid this message
  warnings.warn((
```

Si le hago caso a la *warning* no aparecen más las warnings. De todos modos ojo porque
abajo no dice claramente el resultado de los tests.

Queda entonces intentar instalar ruby. Para ello en `example/tasks/main.yml`

```yaml
---
# tasks file for example
- name: Instala rbenv
  become: yes
  package:
    name: "{{ item }}"
    state: present
  with_items:
    - rbenv

- name: Install ruby 2.7.3
  shell: rbenv install 2.7.3
```

```bash
TASK [example : Instala ruby] **************************************************
failed: [vagrant-ubuntu] (item=rbenv) => {"ansible_loop_var": "item", "changed": false, "item": "rbenv", "msg": "No package matching 'rbenv' is available"}
```

Y no funciona. De todas formas la manera recomendada de instalar rbenv es utilizando
el repositorio. Entonces en `example/tasks/main.yml`

```yaml
---
# tasks file for example
- name: Clone rbenv into ~/.rbenv.
  git:
    repo: https://github.com/rbenv/rbenv.git 
    dest: ~/.rbenv
    update: yes
    force: yes

- name: Add ~/.rbenv/bin to your $PATH for access to the rbenv command-line utility
  shell: echo 'export PATH="$HOME/.rbenv/bin:$PATH"' >> ~/.profile

- name: Set up rbenv in your shell.
  shell: ~/.rbenv/bin/rbenv init

- name: Restart your shell so that PATH changes take effect.
  shell: exec "$SHELL"

- name: Verify that rbenv is properly set up using this rbenv-doctor script
  shell: curl -fsSL https://github.com/rbenv/rbenv-installer/raw/main/bin/rbenv-doctor | bash

- name: Install ruby-build as an rbenv plugin.
  git:
    repo: https://github.com/rbenv/rbenv.git
    dest: $(rbenv root)/plugins/ruby-build
    update: yes
    force: yes

- name: Install ruby 2.7.3
  shell: rbenv install 2.7.3

- name: Check ruby version
  shell: ruby -v
```

Esto tampoco funciona porque seguí al pie de la letra las instrucciones del
repositorio de rbenv, que no es lo que corresponde porque
en ansible las variables de entorno no se manejan de esa manera.

... Horas después:

```yaml
---
# tasks file for example
- name: Clone rbenv into rbenv_root.
  git:
    repo: https://github.com/rbenv/rbenv.git 
    dest: '{{ rbenv_root }}'
    force: yes

- name: Create plugins directory
  file:
    state: directory
    path: '{{ rbenv_root }}/plugins'

- name: Install ruby-build as an rbenv plugin.
  git:
    repo: https://github.com/rbenv/ruby-build.git
    dest: '{{ rbenv_root }}/plugins/ruby-build'
    force: yes

# - name: Add rbenv to system-wide $PATH.
#   file: path=/usr/local/bin/rbenv src='{{ rbenv_root }}'/bin/rbenv state=link

# - name: Add another bin dir to system-wide $PATH.
#   copy:
#     dest: /etc/profile.d/rbenv-path.sh
#     content: 'PATH=$PATH:{{ rbenv_root }}/bin'
#     become: yes

# - name: Add ~/.rbenv/bin to your $PATH for access to the rbenv command-line utility
#   shell: echo 'export PATH="$HOME/.rbenv/bin:$PATH"' >> ~/.profile

# - name: Set up rbenv in your shell.
#   shell: ~/.rbenv/bin/rbenv init

# - name: Restart your shell so that PATH changes take effect.
#   shell: exec "$SHELL"

# - name: Verify that rbenv is properly set up using this rbenv-doctor script
#   shell: curl -fsSL https://github.com/rbenv/rbenv-installer/raw/main/bin/rbenv-doctor | bash


- name: Set up rbenv in PATH.
  shell: "export PATH={{rbenv_root}}/bin:$PATH"

- name: Set up RBENV_ROOT.
  shell: "export RBENV_ROOT={{rbenv_root}}"

- name: rbenv | install ruby-build
  become: yes
  command: './install.sh chdir=/home/vagrant/.rbenv/plugins/ruby-build'
- name: rbenv init.
  shell: eval "$(rbenv init -)"
- name: rbenv rehash.
  shell: '{{rbenv_root}}/bin/rbenv rehash'
#    if [ -d "$HOME/.rbenv" ]; then
#   export PATH=$HOME/.rbenv/bin:$PATH;
#   export RBENV_ROOT=$HOME/.rbenv;
#   eval "$(rbenv init -)";
# fi

#   eval "$({{ rbenv_root }}/bin/rvenv init - bash)"

- name: Install ruby 2.6.7
  shell: '{{rbenv_root}}/bin/rbenv install 2.6.7'

# - name: Check ruby version
#   shell: ruby -v
```

```bash
TASK [example : Install ruby 2.6.7] ********************************************
fatal: [vagrant-ubuntu]: FAILED! => {"changed": true, "cmd": "~/.rbenv/bin/rbenv install 2.6.7", "delta": "0:00:15.031744", "end": "2021-06-16 05:43:27.712067", "msg": "non-zero return code", "rc": 1, "start": "2021-06-16 05:43:12.680323", "stderr": "Downloading ruby-2.6.7.tar.bz2...\n-> https://cache.ruby-lang.org/pub/ruby/2.6/ruby-2.6.7.tar.bz2\nInstalling ruby-2.6.7...\n\nBUILD FAILED (Ubuntu 18.04 using ruby-build 20210611-1-g1b477ae)\n\nInspect or clean up the working tree at /tmp/ruby-build.20210616054313.2143.umC5eX\nResults logged to /tmp/ruby-build.20210616054313.2143.log\n\nLast 10 log lines:\nchecking for ruby... false\nchecking build system type... x86_64-pc-linux-gnu\nchecking host system type... x86_64-pc-linux-gnu\nchecking target system type... x86_64-pc-linux-gnu\nchecking for gcc... no\nchecking for cc... no\nchecking for cl.exe... no\nconfigure: error: in `/tmp/ruby-build.20210616054313.2143.umC5eX/ruby-2.6.7':\nconfigure: error: no acceptable C compiler found in $PATH\nSee `config.log' for more details", "stderr_lines": ["Downloading ruby-2.6.7.tar.bz2...", "-> https://cache.ruby-lang.org/pub/ruby/2.6/ruby-2.6.7.tar.bz2", "Installing ruby-2.6.7...", "", "BUILD FAILED (Ubuntu 18.04 using ruby-build 20210611-1-g1b477ae)", "", "Inspect or clean up the working tree at /tmp/ruby-build.20210616054313.2143.umC5eX", "Results logged to /tmp/ruby-build.20210616054313.2143.log", "", "Last 10 log lines:", "checking for ruby... false", "checking build system type... x86_64-pc-linux-gnu", "checking host system type... x86_64-pc-linux-gnu", "checking target system type... x86_64-pc-linux-gnu", "checking for gcc... no", "checking for cc... no", "checking for cl.exe... no", "configure: error: in `/tmp/ruby-build.20210616054313.2143.umC5eX/ruby-2.6.7':", "configure: error: no acceptable C compiler found in $PATH", "See `config.log' for more details"], "stdout": "", "stdout_lines": []}
```

Lo que pasa es que hay que instalar las [dependencias](https://github.com/rbenv/ruby-build/wiki#suggested-build-environment) antes.
También, probando en una máquina con rbenv ya instalado veo más instrucciones:

```bash
$ ~/.rbenv/bin/rbenv init
# Load rbenv automatically by appending
# the following to ~/.bash_profile:

eval "$(rbenv init - bash)"
```

Entonces voy a reescribir los tasks. Voy a usar un bloque, para no tener que estar dándole permisos a cada task por separado, ya que quiero quiero que se instale system-wide. Además puedo definir variables para no hardcodear rutas ni números de versión.

En `example/vars/main.yml`:

```yaml
---
# vars file for example
rbenv_root: "/usr/local/rbenv"
rbenv_profile: "/etc/profile.d/rbenv-path.sh"
ruby_version: "2.7.3"
```

El bloque de tareas como root quedará en  `example/tasks/main.yml`:

```yaml
---
# tasks file for example
- name: Install rbenv and rbenv-build
  block:
    #Aqui van los tasks
    # -name: ...

  become: true
  become_user: root
  ignore_errors: no
```

Esos tasks serán:

* Instalar [dependencias](https://github.com/rbenv/ruby-build/wiki#suggested-build-environment) (por ahora para rbenv-build).
  
  ```yaml
  - name: Install dependencies
    apt:
      name: ['autoconf', 'bison', 'build-essential', 'libssl-dev', 'libyaml-dev', 'libreadline6-dev', 'zlib1g-dev', 'libncurses5-dev', 'libffi-dev', 'libgdbm5', 'libgdbm-dev', 'libdb-dev', 'git']
      state: 'latest'
      install_recommends: no
  ```

* Clonar los repositorios, ahora utilizando las variables:
  
  ```yaml
  - name: Clone rbenv repo
    git:
      repo: https://github.com/rbenv/rbenv.git 
      dest: '{{ rbenv_root }}'
      force: yes
  
  - name: Create plugins directory
    file:
      state: directory
      path: '{{ rbenv_root }}/plugins'
  
  - name: Install ruby-build as an rbenv plugin.
    git:
      repo: https://github.com/rbenv/ruby-build.git
      dest: '{{ rbenv_root }}/plugins/ruby-build'
      force: yes
  ```

* [Agregar variables de entorno y editar PATH](https://www.jeffgeerling.com/blog/2017/add-path-global-path-ansible).
  
  Como la consola no se recarga entre
  las tareas de ansible, esto fue bastante dificultoso. Lo que se puede hacer es
  crear un archivo en `/etc/profile.d/rbenv-path.sh` que será cargado al iniciar
  las consolas.
  
  Luego se puede realizar un "source" de este archivo para que estén estos
  cambios disponibles inmediatamente. La instrucción `source` no funciona
  correctamente ya que es de `bash` y ansible usa `sh`, en donde se logra el mismo
  resultado con "`.`".
  
  ```yaml
  - name: Add rbenv to system-wide $PATH.
    copy:
      dest: '{{ rbenv_profile }}'
      content: |
          export PATH={{ rbenv_root }}/bin:$PATH;
          export RBENV_ROOT="{{ rbenv_root }}";
          eval "$(rbenv init -)"
  
  - name: Check if ruby is installed.
    shell: $0 -lc "rbenv versions --bare"
    register: rbenv_installed
    changed_when: false
    ignore_errors: yes
    failed_when: false
    check_mode: no
  
  - name: Install ruby {{ ruby_version }}
    shell: '. {{ rbenv_profile }} && rbenv install --skip-existing {{ ruby_version }}'
    when: rbenv_installed.stdout == ""
  ```
  
  La tarea del medio lo que hace es revisar si ya está instalada alguna versión de
  ruby. Si no hay ninguna, la variable `rbenv_installed` queda vacía, y entonces la
  ultima tarea se saltea gracias al `when:` que chequea esa variable.
  
  Esto lo hago porque de lo contrario la última tarea no cumple las pruebas de
  *idempotencia* ya que cuando se usa el `install` por más que se agregue el
  parámetro `--skip-existing` se producen cambios en el sistema.
  Quizás esto se deba al uso del "source" anterior al comando `rbenv install`...
  
  Otra forma:
  
  ```yaml
  - name: Install ruby {{ ruby_version }}
    shell: "rbenv install --skip-existing {{ ruby_version }}"
    args:
      creates: "{{ rbenv_root }}/versions/{{ ruby_version }}/bin/ruby"
    environment:
      CONFIGURE_OPTS: "--disable-install-doc"
      RBENV_ROOT: "{{ rbenv_root }}"
      PATH: "{{ rbenv_root }}/bin:{{ rbenv_root }}/shims:{{ rbenv_root }}/plugins/ruby-build/bin:{{ ansible_env.PATH }}"
  ```
  
  Con esto "aviso" que se van a crear unos archivos, y ademas no hago el "source" ya que especifico el `PATH`

* Por último se puede setear una versión de ruby de manera global. También
  podría hacerlo luego el usuario que lo necesite, pero para probar:
  
  ```yml
  - name: Set ruby version as global
    shell: '. {{ rbenv_profile }} && rbenv global {{ ruby_version }}'
    changed_when: false
  ```

### Roles

En este punto podría decir que ya está funcionando la instalación de `rbenv` y `ruby`, pero para poder correr el Redmine hacen falta otros servicios y configuraciones. Para que no se forme un playbook monolítico enorme, podría dejar todo lo realizado hasta ahora en un rol y crear otros que hagan el resto
Entonces tendré un playbook principal:

```yaml
- hosts: default
  vars:
    - rbenv_root: "/usr/local/rbenv"
    - rbenv_profile: "/etc/profile.d/rbenv-path.sh"
    - ruby_version: "2.7.3"
    - redmine_root: "/usr/local/redmine"
    - redmine_user: redmine
    - mysql_login_user: "redmine"
    - mysql_login_password: "banana"
  roles:
    - ruby
    - mysql
    - redmine
```

> Creo que no se ponen así las variables

Así puedo dividir el despliegue en esos tres pasos. Habrá un directorio para los roles y dentro de ellos puedo correr los tests independientemente.

Entonces creo la carpeta de roles y para crear un rol, hago:

```sh
(venvtest)$ mkdir roles
(venvtest)$ cd roles
(venvtest)$ molecule init role <nombre_rol> --driver-name vagrant --verifier-name testinfra
```

Sin el `--verifier-name`, toma por defecto `ansible`

#### Role: `ruby`

Creado el rol hay que modificar algunos archivos:

* `roles/ruby/tasks/main.yml`: aquí va todo el "playbook" que escribí arriba.

* `roles/ruby/vars/main.yml`: aquí van las variables, en este caso
  
  ```yaml
  ---
  # vars file for ruby
  rbenv_root: "/usr/local/rbenv"
  rbenv_profile: "/etc/profile.d/rbenv-path.sh"
  ruby_version: "2.6.7"
  ```

* `roles/ruby.old/molecule/default/molecule.yml`: Esta es la configuración con la que se van a correr los tests.
  
  ```yaml
  ---
  dependency:
    name: galaxy
  driver:
    name: vagrant
  platforms:
    - name: vagrant-ubuntu
      box: ubuntu/bionic64
  provisioner:
    name: ansible
  verifier:
    name: testinfra
  ```

* `roles/ruby/molecule/default/test_default.py`: Aqui los tests. Por ejemplo:
  
  ```python
  def test_hosts_file(host):
      """Validate /etc/hosts file."""
      f = host.file("/etc/hosts")
  
      assert f.exists
      assert f.user == "root"
      assert f.group == "root"
  ```
  
  Aquí tuve bastantes contratiempos pensando cómo probar que el rol hizo
  lo que tenía que hacer.
  Finalmente, leyendo la [Documentación de Testinfra](https://testinfra.readthedocs.io/en/latest/modules.html#host) encuentro la función
  `find_command()` que busca en el `$PATH` si un comando está disponible.
  Así si se encuentra `ruby` y `rbenv` el rol habría funcionado correctamente.
  
  Pero el problema es que no figuran en el `$PATH`, hay que extenderlo
  con el argumento `extrapaths`. Pero como no quiero hardcodear las rutas, me
  gustaría poder agarrar las variables que están definidas en `vars/main.yml`.
  
  Esto fue bastante engorroso hasta que encontré [Using Ansible variables in testinfra (StackOverflow)
  ](https://stackoverflow.com/questions/46792370/using-ansible-variables-in-testinfra).
  Entonces en `roles/ruby/molecule/default/conftest.py`:
  
  ```python
  @pytest.fixture()
  def AnsibleVars(host):
      ansible_vars = host.ansible("include_vars", "file=main.yml")
      print("ansible_vars ", ansible_vars)
      return ansible_vars["ansible_facts"]
  ```
  
  Y volviendo a `roles/ruby/molecule/default/test_default.py`:
  
  ```python
  def test_ruby(host, AnsibleVars):
    # PATH: "{{ rbenv_root }}/bin:{{ rbenv_root }}/shims:{{ rbenv_root }}/plugins/ruby-build/bin:{{ ansible_env.PATH }}"
    rbenv_root = AnsibleVars['rbenv_root']
    paths = (f"{rbenv_root}/bin", f"{rbenv_root}/shims", f"{rbenv_root}/plugins/ruby-build/bin")
    host.find_command("rbenv", extrapaths=paths)
    host.find_command("ruby", extrapaths=paths)
  ```

#### Role: `mysql`

Cuidado con los paquetes que se instalan, y en que orden, si no:

```sh
TASK [mysql : update mysql root password for all root accounts] ****************
fatal: [vagrant-ubuntu]: FAILED! => {"changed": false, "msg": "The PyMySQL (Python 2.7 and Python 3.X) or MySQL-python (Python 2.X) module is required."}
```

Lo que funcionó:

```yaml
- name: Install Mysql dependencies
  apt:
    name: 
    - mysql-server
    - python-pymysql
    - python3-pymysql
    - python-apt
    - python3-apt
    - libmysqlclient-dev
    update_cache: yes
    cache_valid_time: 3600
    state: 'present'
  become: true
```

Luego para evitar el pasaje de credenciales por variables de entorno,
se utilizo el `socket` en `'/var/run/mysqld/mysqld.sock'`.
Quizás debería parametrizarse esta ruta, y también el nombre de la
base de datos.

```yaml
- name: update mysql password
  mysql_user:
    login_unix_socket: '/var/run/mysqld/mysqld.sock'
    name: "{{ mysql_login_user }}"
    password: "{{ mysql_login_password }}"
    check_implicit_admin: yes
    priv: "*.*:ALL,GRANT"
  become: true

- name: create a new database
  become: true
  mysql_db:
    login_unix_socket: '/var/run/mysqld/mysqld.sock'
    name: "redminedb"
    state: present
```

#### Role: `redmine`

Siguiendo los pasos de la [Guía de Redmine](https://www.redmine.org/projects/redmine/wiki/RedmineInstall), y adaptándolos a ansible se construyen los tasks.

Lo que tiene de distinto de los otros dos, es que este rol depende de los anteriores, por lo tanto
en `roles/redmine/meta/main.yml`:

```yaml
...
dependencies:
  - role: ruby
  - role: mysql
```

Luego de clonar el repositorio, como ya se vio antes, se puede crear un usuario especial para que despliegue el la aplicación y no se haga con el `root`.

```yaml
- name: Add the user "{{ redmine_user }}" with a bash shell
  become: true
  user:
    name: "{{ redmine_user }}"
    shell: /bin/bash

- name: Give permision of {{ redmine_root }} and {{ rbenv_root }}
  become: true
  file:
    path: "{{ item }}"
    state: directory
    recurse: yes
    owner: "{{ redmine_user }}"
    group: "{{ redmine_group }}"
    mode: '0775'
    # for idempotency
    access_time: preserve
    modification_time: preserve
  with_items:
    - "{{ rbenv_root }}"
    - "{{ redmine_root }}"
```

El modo de permiso no estoy seguro, porque necesito escribir y ejecutar con el owner, pero con el grupo y el publico no se que poner.

Los demás tasks son simplemente los pasos de la guía usando el modulo `shell` de ansible.

Un problema encontrado fue que a la hora de realizar el `bundle install` la vm se
quedaba sin memoria. Fallaban los tests, en realidad el aprovisionamiento en el `vagrant up`
entonces accediendo por ssh, con `vagrant ssh` o `molecule create` + `molecule login` en caso
de estar corriendo los tests del rol redmine.

```sh
bundle install
...
Fetching nokogiri 1.11.7 (x86_64-linux)
Installing nokogiri 1.11.7 (x86_64-linux)
Killed
```

Para evitar esto hay que darle mas memoria a la vm, entonces en los archivos
`roles/<nombre_rol>/molecule/default/molecule.yml`:

```yml
...
platforms:
  - name: vagrant-ubuntu
    box: ubuntu/bionic64
    memory: 1024
    cpus: 1
...
```

Eso lo va a tomar a la hora de correr los tests con `molecule`, también habría
que indicarlo en la configuración de `vagrant`:

```ruby
config.vm.provider "virtualbox" do |v|
    v.memory = 1024
    v.cpus = 1
  end
```

Para ver mejor los errores de ansible, que por defecto se imprimen "raw" en la
consola, y dificulta la lectura, hay que cambiar lo que se llama el
`stdout_callback`, según [Use Ansible's YAML callback plugin for a better CLI experience](https://www.jeffgeerling.com/blog/2018/use-ansibles-yaml-callback-plugin-better-cli-experience).

`roles/<nombre_rol>/molecule/default/molecule.yml`:

```yml
...
provisioner:
  name: ansible
  config_options:
    defaults:
      stdout_callback: yaml
...
```

También se puede indicar en la el archivo `ansible.cfg`

```cfg
[defaults]
# Use the YAML callback plugin.
stdout_callback = yaml
# Use the stdout_callback when running ad-hoc commands.
bin_ansible_callbacks = True
```

Un problema problema difícil de reproducir y arreglar fue cuando por
algún motivo desconocido falló uno de los repositorios de Ubuntu. A la
hora de aprovisionar la maquina, en el primer uso del módulo `apt`
fallaba con:

```sh
...
Ign:1 http://archive.ubuntu.com/ubuntu bionic-updates/main amd64 linux-libc-dev amd64 4.15.0-144.148
Err:1 http://security.ubuntu.com/ubuntu bionic-updates/main amd64 linux-libc-dev amd64 4.15.0-144.148
  404  Not Found [IP: 91.189.88.142 80]
...
E: Unable to fetch some archives, maybe run apt-get update or try with --fix-missing?
```

Para intentar seguir la recomendación, la manera de pasar el parámetro
`--fix-missing` al módulo `apt` de ansible es poner `state: 'fixed'`
en lugar de `'present'` o `'latest'`. De todas maneras esto no sirvió
para resolver el error, que luego dejó de ocurrir espontáneamente.

### rails server

La primer manera que se me ocurrió y funciona es

```yaml
- name: Check if server is started
  shell: "cat {{ redmine_root }}/tmp/pids/server.pid"
  register: server_started
  changed_when: false
  ignore_errors: yes
  failed_when: false
  check_mode: no

- name: Rails serve
  environment:
    RAILS_ENV: 'production'
    REDMINE_LANG: 'es'
  shell: bundle exec rails server -u webrick -e production -d
  args:
    chdir: "{{ redmine_root }}"
  when: server_started.stdout == ""
```

El parámetro `-d` demoniza el servidor, para que se libere la terminal. También se podría matar el proceso si está iniciado y luego reiniciarlo. Para ello:

```yaml
- name: Kill server if started
  shell: "kill -9 {{ server_started.stdout }}"
  register: server_terminated
  when: server_started.stdout != ""
```

Pero esto hay que tener cuidado porque el SIGKILL hace que se pierda todo lo que no se bajo a disco.

Para hacerlo mejor habría que crear una unidad se systemd, por ejemplo

`/etc/systemd/system/redmine.service`

```sh
[Unit]
Description= Serve redmine app
Requires=network.target

[Service]
Type=simple
User={{redmine_user}}
Group={{redmine_user}}
WorkingDirectory={{}}
ExecStart=/usr/bin/bash -lc '{{rbenv_root}}/shims/bundle exec rails server -u webrick -e production'
TimeoutSec=30
RestartSec=15s
Restart=always

[Install]
WantedBy=multi-user.target
```

Así, además de hacer correr el servidor de rails, si la maquina se reinicia,
lo volverá a iniciar.

Para habilitar el servicio:

```sh
sudo systemctl daemon-reload
sudo systemctl enable redmine.service
```

Entonces en la forma ansible queda:

```yaml
- name: Configure redmine
  become: yes
  block:

    - name: Setup redmine service
      copy:
        dest: '/etc/systemd/system/redmine.service'
        content: |
          [Unit]
          Description= Serve redmine app
          Requires=network.target

          [Service]
          Type=simple
          User={{redmine_user}}
          Group={{redmine_user}}
          WorkingDirectory={{redmine_root}}
          ExecStart=/bin/bash -lc '{{rbenv_root}}/shims/bundle exec rails server -u webrick -e production'
          TimeoutSec=30
          RestartSec=15s
          Restart=always

          [Install]
          WantedBy=multi-user.target

    - name: ensure redmine is running and starts on boot
      service:
        name: redmine.service
        state: started
        enabled: true
```

## Idempotencia

```yaml
CRITICAL Idempotence test failed because of the following tasks:
*  => ruby : Clone rbenv repo
*  => ruby : Install ruby-build as an rbenv plugin.
*  => redmine : Clone redmine repo
*  => redmine : Give permision of /usr/local/redmine
*  => redmine : Give permision of /usr/local/rbenv
*  => redmine : Bundle config (no dev or test)
*  => redmine : Bundle generate_secret_token
*  => redmine : Bundle db:migrate
*  => redmine : Bundle load_default_data
*  => redmine : Create a directories for assets
*  => redmine : Create a directories for assets
*  => redmine : Create a directories for assets
*  => redmine : Create a directories for assets
*  => redmine : Kill server if started
*  => redmine : Rails serve
```

Lo de los permisos se puede resolver creando previamente
al usuario de deploy y clonando los directorios en el home,
así ya tendría los permisos correctos.

Lo de Bundle tengo que fijarme más a fondo si se puede realizar con algún módulo de ansible que no sea `shell`, o quizás se puede usar el `changed_when: false`. O usar el argumento `creates` indicando que archivos se cambian.

Para el clonado de repositorios no se bien como resolverlo. 
Si no se usa el `force: yes`, entonces falla porque al hacer pull encuentra archivos modificados. Si no, los modifica y falla la idempotencia. Y si no hago nada, por ejemplo chequeando antes que exista la carpeta, entonces no se actualiza el repositorio. Si se usa `changed_when: false` uno no se entera si se actualiza.
