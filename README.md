# Laboratorio 2 - Redmine usando IaC en AWS

## Introduccion

Habiendo desarrollado roles de ansible para el aprovisionamiento
de redmine en una maquina virtual. Ahora se desea llevar la aplicación
a la nube utilizando como proveedor de servicios AWS y automatizando
el despliegue mediante IAC.

Adicionalmente se debe considerar la realizacion de backups y el chequeo
de la salud del servicio con alertas.

## Herramientas

* [Terraform](https://www.terraform.io/): utilizado para la automatización y despliegue de la
infraestructura.

* [Ansible](https://www.terraform.io/): Utilizado para el aprovisionamiento de la infraestructura
creada por terraform.

* [Uptime Robot](https://uptimerobot.com/): Utilizado para realizar el monitoreo de la aplicación.

## Solucion

La arquitectura de la solución consiste en una única instancia ec2 que
una vez aprovisionada tiene corriendo un servidor web (nginx) que actúa como
proxy reverso, quien delega en un servidor de aplicación (Puma) la ejecución de
la aplicación redmine en la misma maquina. Esta instancia tiene a su vez
corriendo un servidor de base de datos (Mysql) que trabaja en conjunto con la
aplicación redmine.

### Estructura de archivos

En la carpeta [terraform](./terraform) encontramos los scripts de configuración
utilizados para crear la infraestructura. Estos archivos son:

* [main.tf](./terraform/main.tf): Es el archivo principal en donde se crea la
instancia EC2.

* [variables.tf](./terraform/variables.tf): Contiene variables que son utilizadas
para el despliegue. Estas variables son:

| nombre | tipo | descripción | default |
| --- | --- | --- | --- |
| instance\_ami | string | El id de la imagen de la instancia | ami-090717c950a5c34d3(ubuntu 18.04) |
| instance\_name | string | El nombre que recibirá la instancia | Redmine-instance |
| instance\_user\_name | string | El nombre de usuario por defecto para conectarse a la instancia | ubuntu |
| pem\_key\_name | string | El nombre de la key para asociar a la instancia | |
| pem\_key\_file | string | Path a la clave privada en el sistema local | |
| region | string | Region de aws para desplegar la infraestructura | us-west-2 |

* [dlm\_role.tf](./terraform/dlm_role.tf): crea roles y políticas de backup.

* [outputs.tf](./terraform/outputs.tf): Se controla la la información que devuelve la ejecución
del despliegue.

* [ansible.tf](./terraform/ansible.tf): Se utiliza para crear un archivo de hosts que será
  suministrado a ansible para el aprovisionamiento.

* [security\_groups.tf](./terraform/security_groups.tf): Configura security groups para permitir
el acceso por http y ssh a la instancia creada.

* [/templates/ansible.cfg.tpl](./terraform/templates/ansible.cfg.tpl): Template utilizado para
crear el archivo de configuración para utilizar con ansible.

* [/templates/hosts.tlp](./terraform/templates/hosts.tpl): Template utilizado para la creación
del archivo de hosts para utilizar con ansible.

En la carpeta [ansible](./ansible) una vez desplegada la infraestructura se encontrara el
archivo de hosts que apunta a la instancia creada para su aprovisionamiento.

En la carpeta [monitoring](./monitoring) encontramos la logica para agregar el monitoreo
a la aplicación. En esta carpeta encontramos dos archivos:

* [monitoring/create-monitor.js](./monitoring/create-monitor.js): este script sirve para
crear un monitor http en la plataforma uptime robot. Recibe como parámetros posicionales:

| posición | rol | formato | descripción |
| --- | --- | --- | --- |
| 0 | host | string |el host que se desea monitorear |
| 1 | prefijo del nombre | string | se usa como prefijo en el nombre del monitor. Los nombres de los monitores se crean utilizando un prefijo y el host recibido como parámetro |
| 2 | id de contactos | string separados por coma | Son los ids de los contactos para las alertas del monitor. |

Por ejemplo, para crear un monitor de www.google.com con prefijo `mimonitor` y con contactos 012463 y 13524:

```sh
node monitoring/create-monitor.js www.google.com mimonitor 012463,13524
```

* [monitoring/delete-monitor.js](./monitoring/delete-monitor.js): este script sirve para
borrar un monitor de la plataforma uptime robot. Recibe como parámetro requerido el nombre del
monitor. Por ejemplo para borrar el monitor anterior:

```sh
node monitoring/delete-monitor.js minonitor-www.google.com 
```

## Set up

### Terraform

Suponemos que ya se encuentra instalado en el ambiente local **Terraform** y **aws-cli**.
En caso de necesitar instalarlos se pueden seguir las instrucciones:

* [Instalacion de terraform](https://registry.terraform.io/providers/hashicorp/aws/latest)
* [Instalacion de aws-cli](https://docs.aws.amazon.com/cli/latest/userguide/install-cliv2.html)

Para poder desplegar la infraestructura correctamente y que pueda ser aprovisionada correctamente
deben setearse dos variables de entorno:

```sh
export TF_VAR_pem_key_name=<nombre de la key para asociar a la instancia>
export TF_VAR_pem_key_path=<path a la key en el fs local>
```

Adicionalmente pueden sobreescribirse las demás variables mencionadas en la explicación de
terraform siguiendo el patrón **TF\_VAR\_nombre\_de\_variable**_.

En caso de no setear estas variables de entorno, es posible especificarlas en la ejecución
del comando terraform apply de la siguiente manera:

```sh
terraform apply -var="pem_key_name=<nombre de la key>" -var="pem_key_path=<path a la key>"
```

### Aprovisionamiento

Primero creamos y activamos un ambiente virtual python en la carpeta del proyecto. En este
paso utilizamos [pipenv](https://pypi.org/project/pipenv/) pero con cualquier manejador de
ambientes locales funciona:

```sh
pipenv shell
```

Habiendo hecho esto, instalamos dependencias (ansible):

```sh
pipenv install
```

Habiendo hecho esto tendremos disponible el comando ansible para ejecutar en el ambiente local.

### Monitoreo

Luego instalamos las dependencias para la parte de monitoreo. Los scripts se encuentran escritos
en javascript utilizando la libreria [uptime-robot](https://www.npmjs.com/package/uptime-robot)
que actúa de intermediario con la api de la plataforma.

Entonces la instalamos:

```sh
npm i
```

Luego debemos obtener una api key para comunicarnos con la api. Para ello debemos [crear una cuenta](https://uptimerobot.com/signUp)
(si no tenemos una) y obtener la key en el apartado de settings.

Finalmente debemos setear una variable de entorno con dicha key:

```sh
export UPTIMEROBOT_API_KEY=xxXxXxXxXXXXXx
```

Habiendo hecho esto ya sera posible correr los scripts mencionados en la explicación de monitoreo.

## Despliegue

Habiendo preparado el ambiente. Para realizar el despliegue basta con ejecutar:

```sh
cd terraform
terraform/$ terraform apply
```

El comando generara un plan de accion y nos pedira confirmacion. Habiendo visto que el plan
es acorde a lo que se espera se le da confirmacion y se crea la infraestructura junto con los 
monitores y los archivos que necesita ansible para ejecutarse.

Ahora aprovisionamos la instancia:

```sh
ansible-playbook playbook.yml
```

Cuando termina su ejecución, ya tenemos disponible la aplicación en la ip que nos mostró terraform
en el paso anterior.

## Backups

No es necesario realizar ninguna acción adicional. Para configurar la política de backups se puede
editar el archivo [`dlm_role.tf`](./terraform/dlm_role.tf).
En este caso se define que se guarde una Snapshot por día y se mantienen durante dos semanas.

> Nota en caso de destruir la infraestructura, se elimina la política, es decir no se crearán nuevos snapshots, pero tampoco se eliminan aquellos ya creados.

Para restaurar una instancia a partir de un Snapshot guardado ver [backup.md](./backup.md)
