# Monitoreo

El monitoreo de la aplicacion consiste en chequear que la misma devuelva un 200/OK
cada 5 minutos. En caso de que el codigo devuelto no sea OK, se envia una alerta 
por email que avisa que el servicio esta caido. De igual manera, cuando se reestablece
se envia una alerta indicando que ya se encuentra activo nuevamente.

## Herramientas

Para realizar el monitoreo se utiliza [uptime robot](https://uptimerobot.com/). Dicha
plataforma, en su version gratuita permite establecer hasta 50 monitores con distintas
formas de chequeo (ICMP, HTTP, keyword, entre otros). Permite realizar el monitoreo cada
5 minutos y provee un dashboard donde ver todos los monitores y su estado.

Ademas provee una [API REST](https://uptimerobot.com/api/) para interactuar con la 
aplicacion, lo que resulta muy conveniente para crear y borrar monitores automaticamente.

Tambien se utiliza una libreria JS llamada [uptime-robot](https://www.npmjs.com/package/uptime-robot)
que funciona como un wrapper para comunicarse con la api mencionada.

## Funcionamiento

Hay dos scripts, [create-monitor.js](./create-monitor.js) y [delete-monitor.js](./delete-monitor.js),
que se encargan de crear y borrar monitores respectivamente.

Estos scripts reciben como parametro requerido un host (en este caso una direccion ip) y opcionalmente
un email de contacto; de forma que al ejecutarse crean un monitor HTTP asociado al host (o lo borran).

## Set up

Para correr los scripts es necesario instalar las dependencias.

```sh
npm install
```

Setear una variable de entorno con la api key que suministra la plataforma a los usuarios:

```sh
export UPTIME_ROBOT_API_KEY=<your_api_key>
```

Finalmente correr los scripts:

```sh
node monitoring/create-monitor.js google.com

# o bien
node monitoring/delete-monitor.js google.com
```
