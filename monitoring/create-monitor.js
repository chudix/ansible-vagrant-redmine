var UptimeRobot = require('uptime-robot');
// TODO: make alertContact dynamic
//
const apiKey = process.env.UPTIMEROBOT_API_KEY

// Uptime robot parameters
var alertContact,host, name;

// First arg (required): the host to monitor (an ip or name)
// Second arg (optional): a contact alert
var args = process.argv.slice(2)
host = args[0]
name = args[1] || "default"
contactIds = args[2] || "01372043"
// if no contact supplied use default

var client = new UptimeRobot(apiKey)
client.newMonitor({friendlyName:`${name}-${host}`, url: `http://${host}`, alertContacts: [contactIds]});

