var UptimeRobot = require('uptime-robot');

const apiKey = process.env.UPTIMEROBOT_API_KEY
const args = process.argv.slice(2)
var client = new UptimeRobot(apiKey)

const monitorName = args[0]

client.getMonitors().then(res => {
  return res.filter(monitor => monitor.friendlyname == `${monitorName}`).pop()
  })
  .then( monitor => {
    monitor && client.deleteMonitor(monitor.id)
  })
  .catch(e => console.log(e))
