# Correcciones

El rol se ejecuta perfectamente con las instrucciones brindadas en el readme. Al crear
y aprovisionar la vm, encontramos corriendo la aplicacion en el puerto 3000 del host
como es esperado.

## Analisis

Al revisar el codigo se encontraron detalles que podrian ser mejorados:

* Existen paquetes cuya version no esta fijada. Es deseable que se especifique para asegurar que
el rol funcione a lo largo del tiempo; es decir que si lo ejecutamos el dia de maniana
donde es muy probable que los paquetes de los cuales depende sean actualizados; siga 
funcionando como se espera.

* Existen tareas cuya idempotencia esta fijada con una clausula *changed\_when: false*, lo que
hace que sin importar lo que suceda ansible tome dicha tarea como no cambiada aunque se esten
realizando cambios.

* Se instala rbenv en un directorio general mediante el usuario root y se agrega la inicializacion
en */etc/profiles.d/rbenv_profile.sh*. Esto hace que cualquier usuario que se loguee al sistema 
mediante una terminal de login tenga acceso a ruby y pudiendo ejecutar entre otras cosas la aplicacion.

* Hay tareas que podrian ser parametrizadas agregando variables, constantes y utilizando templates. Tal
es el caso de la creacion del servicio redmine y la creacion del archivo de configuracion de la base
de datos.

## Cambios

### Propuesta

Para solucionar el versionado de los paquetes se pueden agregar variables que indiquen la version deseada.
Dichas variables se establecen en valores por defecto que sabemos funcionan en conjunto, las cuales pueden
ser modificadas por el usuario bajo su responsabilidad.

Para solucionar la idempotencia se debe identificar que cosas cambian cuando se ejecuta un shell command y
agregar clausulas a las tareas que verifiquen la idempotencia.

Para aislar la ejecucion de ruby se puede crear un usuario especifico en el rol, en donde se crearan las 
carpetas correspondientes con sus respectivos permisos en conjunto con la inicializacion especifica en el
archivo *~/.profile* lo que deja disponible los comandos para ese usuario especifico.
 

### Solucion

#### Versionado Mysql

Para el versionado de motor de base de datos se creo una constante con todos los paquetes necesarios y versionados.
Dicha constante se llama **MYQSQL_PACKAGES**.

Adicionalmente se creo otra constante para ubicar al socket de conexion con la base de datos, tambien una variable 
que determina un nombre para crear una base de datos vacia que luego sera utilizada por la aplicacion.

### Versionado de Redmine

En forma similar al anterior se creo una variable llamada **redmine_version** cuyo valor por defecto es 4.2.0. Como
redmine se instala a partir de git, se especifica el tag en la tarea para que clone la version especifica de la
aplicacion.

### Aislamiento de ruby

Para lograr el aislamiento en cuanto a la ejecucion de ruby se crea un usuario que recibe su nombre y password
mediante variables llamadas **ruby_user** y **ruby_userpswd** respectivamente. Estas variables llevan como valor
por defecto **rubist**. El password toma por defecto el nombre de usuario.

Entonces se realiza la instalacion de rbenv y ruby mediante git bajo el usuario creado previamente y 
se cambia el modo de los directorios y archivos para que no puedan ser ejecutados por otros.

Por ultimo se cambia la inicializacion de rbenv y se mueve del archivo general al archivo profile particular
del usuario.

### Idempotencia

En principio se elimino de todas las tasks la clausula mencionada y fueron siendo reemplazadas por argumentos que
le informan a ansible el estado de la tarea. Por ejemplo:

* Al setear la version global de ruby con rbenv, este crea el archivo version dentro de la carpeta de instalacion
de rbenv. Por lo que con solo agregar el parametro *creates* en la tarea basta para que ansible sepa si hay
modificaciones.

* La ejecucion de las migraciones y el seed de la base de datos, siempre arrojan cambios ya que no habria una forma
evidente de decirle a ansible si hay cambios en la base de datos. Para ello se llevaron ambas tareas a handlers, estos
se ejecutan siempre que cambie el archivo de configuracion de la base de datos.

Otras tareas no resultaron tan triviales, tal es el caso de la tarea:

```yml

      - name: Give permision of {{ redmine_root }} and {{ rbenv_root }}
        become: true
        file:
          path: "{{ item }}"
          state: directory
          recurse: yes
          owner: "{{ redmine_user }}"
          group: "{{ redmine_group }}"
          mode: '775'
          # for idempotency
          access_time: preserve
          modification_time: preserve
        with_items:
          - "{{ rbenv_root }}"
          - "{{ redmine_root }}"
```
Que cambia no solo el ownership y grupo del directorio en forma recursiva, sino que cambia tambien el modo. Entonces git
entendia que los archivos habian cambiado por lo que al ser corrida nuevamente se debia clonar el repositorio nuevamente
generando cambio de ownership y reejecucion de la tarea. Con solo quitar el modo la tarea resulta idempotente ya que el 
repositorio permanece limpio, no se clona nuevamente y no es necesario cambiar el ownership.

**Nota:** para esta tarea especifica tuve que agregar una guarda extra ya que ansible por algun motivo marcaba cambios en el
fs mas alla de que los archivos eran exactamente los mismos antes y despues de la ejecucion. Como contexto al activar diff mode
marcaba como diferencia el path del directorio que, tanto en la key "after" como "before", era exactamente el mismo.

### Refactorizacion general

Se cambiaron las tareas que crean archivos para que estos se creen a traves de templates. Tal es el caso de la tarea 
que configura la conexion a la base de datos o la tarea que crea el archivo para dar de alta el servicio redmine.

Tambien se agregaron variables y constantes en cada rol para parametrizar los mismos.

Por ultimo, se modifico la forma en que se instalan paquetes para separarlo del sistema operativo subyacente. Se modificaron las tasks
para que utilicen el modulo package y se agregaron defaults segun la familia del SO. De forma tal que con solo agregar dos archivos
se pueda ejecutar en otro sistema operativo que no este basado en debian.

Actualmente esta probado en debian aunque se encuentra planteado tambien para la familia RedHat.

