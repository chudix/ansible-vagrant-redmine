# Backups

## Problema

Permisos necesarios para crear roles y políticas:

```aws
iam:CreateRole
dlm:CreateLifecyclePolicy
dlm:UpdateLifecyclePolicy
dlm:DeleteLifecyclePolicy
iam:AttachRolePolicy
iam:PutRolePolicy
iam:DeleteRole
```

Solución para evitar esos permisos: Usar rol por defecto

```aws
arn:aws:iam::333591686509:role/service-role/AWSDataLifecycleManagerDefaultRole
```

## Lecturas

* [Data Source: aws_caller_identity](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/caller_identity)

* [Create AWS Lifecycle Policy Using Terraform for Taking EBS Snapshots](https://blog.cloudthat.com/create-aws-lifecycle-policy-using-terraform-for-taking-ebs-snapshots/)

* [Resource: aws_dlm_lifecycle_policy](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/dlm_lifecycle_policy)

* [VOLUME VS INSTANCE VS IMAGE](https://stackoverflow.com/questions/7927057/amazon-ec2-difference-between-images-and-volumes)

## Dudas

**¿Debería crear un snapshot al levantar?**

No, no tiene sentido crear una imagen vacía, siempre se puede destruír y crear de nuevo la instancia.

**¿Debería fijarme si hay un snapshot antes de levantar?**

Tienta un poco la idea de ahorrarse toda la instalación y aprovisionamiento si ya hay un "backup" guardado. Pero eso en última instancia debería ser una decisión no automática.

## Restore

Para restaurar la instancia desde uno de los snapshots usados como back se puede realizar desde la web de AWS o desde la `aws-cli`. El workflow es el [siguiente](https://docs.aws.amazon.com/prescriptive-guidance/latest/backup-recovery/restore.html)

Create new volume from snapshot -> Stop instance -> Volumen detach -> New volume attach -> Start instance

### CLI

1. See snapshots: [describe-snapshots](https://awscli.amazonaws.com/v2/documentation/api/latest/reference/ec2/describe-snapshots.html)

    En este caso la política definida en terraform le asigna un tag a los snapshots,
    justamente para que sea mas sencillo encontrarlos:

    ```bash
    aws ec2 describe-snapshots --filters Name=tag:Name,Values=redmine
    ```

    Otra forma es buscar las políticas

    ```bash
    $ aws dlm get-lifecycle-policies
    Policies:
    - Description: DLM lifecycle policy
      PolicyId: policy-07b7cd40e15e14057
      PolicyType: EBS_SNAPSHOT_MANAGEMENT
      State: ENABLED
      Tags: {}
    ```

    **`export PolicyId=policy-07b7cd40e15e14057`**

    Y luego buscar por ese tag:

    ```bash
    $ aws ec2 describe-snapshots --filters Name=tag:aws:dlm:lifecycle-policy-id,Values=$PolicyId
    Snapshots:
    - Description: 'Created for policy: policy-07b7cd40e15e14057 schedule: 2 weeks of
        daily snapshots'
      Encrypted: false
      OwnerId: '333591686509'
      Progress: 100%
      SnapshotId: snap-036270606fc160493
      StartTime: '2021-07-16T00:05:33.089000+00:00'
    ...
    ```

    Lo importante es obtener el Id del snapshot que se quiere restaurar

    **`export SnapshotId=snap-08c4f9e0bf51fbdd0`**

1. Get info of volume to replace:

    Instance Id (obtained from Terraform Output):

    **`export InstanceId=i-09bf802515e1fc48d`**

    ```bash
    $ aws ec2 describe-volumes --filters Name=attachment.instance-id,Values=$InstanceId --query "Volumes[*].{ID:VolumeId,AZ:AvailabilityZone}"
    - AZ: us-west-2b
      ID: vol-072981e7fcabe00a1
    ```

    Se necesita la AZ porque el nuevo volumen hay que crearlo en la misma. El id lo necesitamos para quitarselo a la instancia

    **`export AvailabilityZone=us-west-2b`**

    **`export OldVolume=vol-072981e7fcabe00a1`**

    ```bash
    $ aws ec2 describe-instances --instance-ids $InstanceId --query "Reservations[].Instances[].BlockDeviceMappings[]"

    - DeviceName: /dev/sda1
      Ebs:
        AttachTime: '2021-07-16T05:08:37+00:00'
        DeleteOnTermination: false
        Status: attached
        VolumeId: vol-072981e7fcabe00a1
    ```

    También se necesita este "punto de montaje", para montar el nuevo volumen en el mismo lugar

    **`export DeviceName=/dev/sda1`**

1. Stop Instance:

    ```bash
    aws ec2 stop-instances --instance-ids $InstanceId
    aws ec2 wait instance-stopped --instance-ids $InstanceId
    ```

1. Detach volume

    ```bash
    aws ec2 detach-volume --instance-id $InstanceId --volume-id $OldVolume
    ```

1. Create New Volume

    ```bash
    $ aws ec2 create-volume --availability-zone $AvailabilityZone --snapshot-id $SnapshotId 

    ...

    VolumeId: vol-0f5f772dbf417393f
    ```

    **`export NewVolume=vol-0f5f772dbf417393f`**

1. Attach new volume

    ```bash
    # Wait old volume is detached
    $ aws ec2 wait volume-available --volume-ids $OldVolume
    # Check new volume is created
    $ aws ec2 wait volume-available --volume-ids $NewVolume
    # Attach new volume
    $ aws ec2 attach-volume --device $DeviceName --instance-id $InstanceId --volume-id $NewVolume
    $ aws ec2 wait volume-in-use --volume-ids $NewVolume
    ```

1. Start instance

    ```bash
    aws ec2 start-instances --instance-ids $InstanceId \
    && aws ec2 wait instance-status-ok --instance-ids $InstanceId
    ```

    Instance should be up and running

1. Post Restore

    El problema es que en nuestro caso no utilizamos ip estáticas, AWS cambia la ip
    al detener y arrancar la instancia.

    Para resolver algunas de estas cosas se puede volver a correr `terraform apply`.
    Los recursos que se hayan definido con dependencias hacia el ip de la instancia,
    se redefinirán

    ```diff
    $ terraform apply

    ...

    # uptimerobot_monitor.main will be updated in-place
    ~ resource "uptimerobot_monitor" "main" {
        ~ friendly_name       = "Redmine-54.189.149.111" -> "Redmine-54.188.44.118"
          id                  = "788714936"
        ~ url                 = "http://54.189.149.111" -> "http://54.188.44.118"

  ```
