variable "instance_ami" {
  description = "ami to use in the EC2 instance"
  type        = string
  default     = "ami-090717c950a5c34d3"
}

variable "instance_name" {
  description = "Value of the Name tag for the EC2 instance"
  type        = string
  default     = "Redmine-instance"
}

variable "instance_user_name" {
  description = "Value of the default user for the EC2 instance"
  type        = string
  default     = "ubuntu"
}

variable "pem_key_name" {
  description = "Name of the key created for ssh access"
  type        = string
}

variable "pem_key_file" {
  description = "Location of pem file"
  type        = string
}

variable "region" {
  description = "AWS Region for the instance"
  type = string
  default = "us-west-2"
}