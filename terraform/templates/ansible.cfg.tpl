[defaults]
host_key_checking = False
inventory = ansible/hosts
remote_user = ${ vm_user_name }

[ssh_connection]
# control_path=%(directory)s/%%h-%%r
# control_path_dir=~/.ansible/cp
# pipelining = True
scp_if_ssh = True
