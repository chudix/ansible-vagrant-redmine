output "instance_id" {
  description = "ID of the EC2 instance"
  value       = aws_instance.app_server.id
}

# This does not work as intended. Currently the only
# way to know what is the default user is reading the
# selected ami documentation.
output "default_user" {
  description = "Default user name of the instance"
  value       = aws_instance.app_server.user_data
}

output "instance_public_ip" {
  description = "Public IP address of the EC2 instance"
  value       = aws_instance.app_server.public_ip
}

output "instance_public_dns" {
  description = "Public DNS of the EC2 instance"
  value       = aws_instance.app_server.public_dns
}

