resource "aws_security_group" "allow_http" {
  name        = "Allow_http"
  description = "Allow HTTP connections from anywhere"


  ingress {
    description = "HTTP"
    from_port = 80
    to_port = 80
    protocol = "tcp"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }

  tags = {
    Name = "HTTP"
  }
}

resource "aws_security_group" "allow_ssh" {
  name        = "Allow_ssh"
  description = "Allow SSH connections from anywhere"


  ingress {
    description = "Allow ssh from anywhere"
    from_port = 22
    to_port = 22
    protocol = "tcp"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  } 

  tags = {
    Name = "SSH"
  }
}

resource "aws_security_group" "allow_all_egress" {
  name        = "Allow_all_outside"
  description = "Allow any packet to exit"


  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }

  tags = {
    Name = "All_outside"
  }
}
