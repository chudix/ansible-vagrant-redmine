terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.27"
    }
    uptimerobot = {
      source = "louy/uptimerobot"
      version = "0.5.1"
    }
  }

  required_version = ">= 0.14.9"
}

provider "aws" {
  profile = "default"
  region  = var.region
}

resource "aws_instance" "app_server" {
  ami           = var.instance_ami
  instance_type = "t2.micro"
  key_name      = var.pem_key_name
  security_groups = [
    aws_security_group.allow_http.name,
    aws_security_group.allow_ssh.name,
    aws_security_group.allow_all_egress.name
  ]
    
  tags = {
    Name = var.instance_name
  }

  # Run custom scripts for monitor creation:
  # Use these in case more functionality than the provided by louy/uptimerobot
  # is needed
  #
  # provisioner "local-exec" {
  #   command = "node ../monitoring/create-monitor.js ${self.public_ip}"
  # }

  # provisioner "local-exec" {
  #   command = "node ../monitoring/delete-monitor.js ${self.public_ip}"
  #   when = destroy
  # }
}

# Sketchy way to run a provisioner AFTER instance creation
# resource "null_resource" "app_server" {
#   provisioner "local-exec" {
#     working_dir = "../"
#     command = "ansible-playbook playbook.yml"
#   }
#   depends_on = [
#     aws_instance.app_server,
#   ]
# }
