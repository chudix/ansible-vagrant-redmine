resource "local_file" "ansible_hosts" {
  content = templatefile("${path.module}/templates/hosts.tpl", {
    vm_public_dns = aws_instance.app_server.public_dns,
    vm_key_file   = var.pem_key_file
  })
  filename = "../ansible/hosts"
}

resource "local_file" "ansible_cfg" {
  content = templatefile("${path.module}/templates/ansible.cfg.tpl", {
    vm_user_name  = var.instance_user_name,
    vm_public_dns = aws_instance.app_server.public_dns,
    vm_key_file   = var.pem_key_file
  })
  filename = "../ansible.cfg"
}