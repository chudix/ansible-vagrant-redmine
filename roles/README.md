# Ansible roles for deploying Redmine with Vagrant

## Requirements

* Vagrant: [Download Vagrant](https://www.vagrantup.com/downloads)

* Python 3 (recomended with virtualenv)
  
  ```sh
  $ sudo apt install python3, python3-venv
  $ python3 -m venv .venv
  $ source venvtest/bin/activate
  ```

## Install

```sh
(.venv)$ git clone git@gitlab.com:juanpsm/ansible-vagrant-redmine.git
(.venv)$ cd ansible-vagrant-redmine
(.venv)$ git pip install -r requirements.txt
```

## Run

```sh
(.venv)$ vagrant up
```

Also, to re-provision

```sh
(.venv)$ vagrant provision
```

Once it finishes, go to [0.0.0.0:3000](http://localhost:3000/)

## Config

Change the values of variables in:

* `roles/ruby/vars/main.yml`
  
  * `rbenv_root`: Directory where to install rbenv (`"/usr/local/rbenv"`)
  
  * `rbenv_profile`: Where to save the updated $PATH (`"/etc/profile.d/rbenv-path.sh"`)
  
  * `ruby_version`: Target ruby version (`"2.6.7"`)

* `roles/mysql/vars/main.yml`
  
  * `mysql_login_password`: Super secure password (`'banana'`)
  
  * `mysql_login_user`: Self explanatory (`'redmine'`)

* `roles/redmine/vars/main.yml`
  
  * `redmine_root`: Where to clone redmine repo (`"/usr/local/redmine"`)
  
  * `redmine_user`: Username used for deploying (`"redmine"`)
  
  * `redmine_group`: Group used for deploying (`"redmine"`)

## Tests (not much yet)

To run molecule instances individually

```sh
(.venv)$ cd roles/<role_name>
(.venv)$ molecule create    # create instance
(.venv)$ molecule converge  # provision (tasks/main.yml)
(.venv)$ molecule verify    # run tests
(.venv)$ molecule test      # all of above
```

## Development Notes (in spanish)

* [Laboratorio 01](lab1.md)

* [Resumen de instalación de Redmine](instalar-redmine.md)

## Correcciones

Las correcciones realizadas pueden verse en [aca](correcciones.md).
