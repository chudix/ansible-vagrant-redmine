---
# tasks file for ruby
- name: load defaults according to os family
  include_vars:
    file: "defaults/{{ ansible_os_family }}.yml"

- name: create user for ruby
  user:
    name: "{{ ruby_user }}"
    shell: /bin/bash
    update_password: on_create
    password: "{{ ruby_userpswd | password_hash('sha512') }}"
    state: present
  become: yes

- name: update package manager cache
  include_tasks: "{{ ansible_os_family }}_update_cache.yml"

- name: install dependencies
  package:
    name: "{{ DEPENDENCIES }}"
    state: present
  become: yes

- name: Install rbenv and rbenv-build
  become: yes
  become_user: "{{ ruby_user }}"
  environment:
    PATH: "{{ RBENV_SHIMS }}:{{ RBENV_PATH }}:{{ ansible_env.PATH }}"
    RBENV_ROOT: "{{ rbenv_root }}"
  block:
    - name: create ansible tmp dir manually
      file:
        state: directory
        path: "/home/{{ ruby_user }}/.ansible/tmp"
        recurse: yes

    - name: Clone rbenv repo
      git:
        repo: "{{ RBENV_REPO }}" 
        dest: "{{ rbenv_root }}"
        force: yes

    - name: Create plugins directory
      file:
        state: directory
        path: "{{ rbenv_root }}/plugins"

    - name: Install ruby-build as an rbenv plugin.
      git:
        repo: "{{ RUBY_BUILD_REPO }}" 
        dest: "{{ rbenv_root }}/plugins/ruby-build"
        force: yes

    - name: Add rbenv to system-wide $PATH
      blockinfile:
        block: "{{ lookup('template', './rbenv_profile.j2') }}"
        path: "{{ rbenv_profile }}"

    - name: Install ruby {{ ruby_version }}
      shell: 
        cmd: "rbenv install --skip-existing {{ ruby_version }}"
        creates: "{{ rbenv_root }}/versions/{{ ruby_version }}/bin/ruby"
      environment:
        CONFIGURE_OPTS: "--disable-install-doc"

    - name: Set ruby version as global
      shell: 
        cmd: "rbenv global {{ ruby_version }}"
        creates: "{{ rbenv_root }}/version"

- name: make ruby executable only by {{ ruby_user }} user and {{ ruby_group }} group
  file:
    path: "{{ rbenv_root }}"
    state: directory
    recurse: yes
    mode: "o-x"
  become: yes
